// import './ckeditor.js';
const Util = require('./util.js');
import './summernote.js'
const Routing = require('./routing.js');


let timeoutId = null;

$('.editor').on('summernote.change', function (we, contents, $editable) {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {

        let count = Util.wordCount(contents);
        $('#word-count').text(count.words);
        saveTopic();
    }, 1500);
});

$('input, textarea').on('input', () => {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        saveTopic();
    }, 1500);
});

$('select').on('change', () => {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        saveTopic();
    }, 1500);
});




function saveTopic() {

    console.dir('autosave');
    // return;

    if (!$('#topic_title').val()) {
        $('#topic-saved').show().html('Please set a title');
        return;

    }
    let blog_id = $('#blog-id').data('id');
    let topic_id = $('#topic-id').data('id');

    $.ajax({
        type: "POST",
        url: Routing.generate('topic_edit', {blog_id: blog_id, topic_id: topic_id}),
        data: $('form').serialize(),

        beforeSend: function () {
            $('.topic-saved').hide();
            $('.topic-saving').show();
        },
        error: function () {
            $('.topic-saving').hide();
            $('.topic-saved').show().html('error: unable to autosave');

        },
        success: function (result) {
            $('.topic-saving').hide();
            $('.topic-saved').show().html('Last autosaved at: ' + new Date().toLocaleString());

            if (!topic_id) {
                console.dir('new topic - setting id to div');
                $('#topic-id').data('id', result.topic_id);

                let url = window.location.href + '/' + result.topic_id;
                window.history.replaceState(null,"", url);

            }
        }
    });

}

require('webpack-jquery-ui/sortable');
require('webpack-jquery-ui/draggable');
require('webpack-jquery-ui/droppable');

$(function () {

    var blog_id = $('#blog').data('id');

    $(".sortable").sortable({
        revert: true,
        // placeholder: "ui-state-highlight",
        handle: '.handle',
        forcePlaceholderSize: true,
        cursor: "move",
        tolerance: 'pointer',

        update: function (event, ui) {
            // var data = $('#blog').serialize();
            var data = $('.sortable').sortable('serialize');

            console.dir(data);

            // POST to server using $.post or $.ajax
            $.ajax({
                data: data,
                type: 'POST',
                url: Routing.generate('topic_update_order', {blog_id: blog_id}),
                error: function () {
                    console.dir('error')

                },
                success: function (result) {
                    console.dir('saved');

                }
            });
        }
    });
});

$('.btn-topic-delete').on('click', function () {

    let id = $(this).data('id');
    let blog_id = $(this).data('blog-id');
    let $this = $(this);

    $.ajax({
        type: "DELETE",
        url: Routing.generate('topic_delete', {blog_id: blog_id, topic_id: id}),

        beforeSend: function () {
        },
        error: function () {
        },
        success: function (result) {
            $this.closest('tr').remove();
        }
    });
});
