// loads the Bootstrap jQuery plugins
// import 'bootstrap-sass/assets/javascripts/bootstrap/collapse.js';
// import 'bootstrap-sass/assets/javascripts/bootstrap/dropdown.js';
// import 'bootstrap-sass/assets/javascripts/bootstrap/modal.js';
// import 'bootstrap-sass/assets/javascripts/bootstrap/transition.js';
require('bootstrap');

import '@fortawesome/fontawesome/index.js';
import '@fortawesome/fontawesome-pro-regular/index.js';
import '@fortawesome/fontawesome-pro-light/index.js';
import '@fortawesome/fontawesome-pro-solid/index.js';

window.$ = $;
