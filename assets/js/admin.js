// import './ckeditor.js';


$('#btn-deploy').on('click', function () {

    $(this).button('loading');

    $.ajax({
        type: "POST",
        url: Routing.generate('admin_deploy'),

        beforeSend: function () {
            $('#block-loading').show();
            $('#btn-deploy').button('loading');
            $('#btn-deploy').prop('disabled', true);



            $('#git-status').html('');
        },
        error: function () {
            $('#block-loading').hide();
            $('#btn-deploy').button('reset');
            $('#btn-deploy').prop('disabled', false);
        },
        success: function (result) {
            $('#block-loading').hide();
            $('#btn-deploy').button('reset');
            $('#btn-deploy').prop('disabled', false);
            $('#git-status').html(result);
        }
    });

    return false;
});

