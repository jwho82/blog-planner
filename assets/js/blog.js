// import './ckeditor.js';
import './summernote.js'

const Util = require('./util.js');
let timeoutId = null;


$('#blog_title').on('input change', function () {
    let length = $(this).val().length;
    $('#title-length').text(length);

    if (length < 10 || length > 60) {
        $('#title-length').removeClass('bg-success').addClass('bg-warning');
    } else {
        $('#title-length').removeClass('bg-warning').addClass('bg-success');
    }
});

$('#blog_meta_description').on('input change', function () {
    let length = $(this).val().length;

    $('#meta-length').text(length);

    if (length < 50 || length > 300) {
        $('#meta-length').removeClass('bg-success').addClass('bg-warning');
    } else {
        $('#meta-length').removeClass('bg-warning').addClass('bg-success');
    }
});

$('#blog_title').change();
$('#blog_meta_description').change();

$('.editor').on('summernote.change', function (we, contents, $editable) {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {

        let count = Util.wordCount(contents);
        $('#word-count').text(count.words);
        saveBlog();
    }, 1500);
});

$('input, textarea').on('input', () => {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        saveBlog();
    }, 1500);
});

$('select').on('change', () => {

    // auto save & word count
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        saveTopic();
    }, 1500);
});

function saveBlog() {

    if (!$('#blog_title').val()) {
        $('#blog-saved').show().html('Please set a title');
        return;
    }

    let blog_id = $('#blog-id').data('id');

    $.ajax({
        type: "POST",
        url: Routing.generate('blog_edit', {blog_id: blog_id}),
        data: $('form').serialize(),

        beforeSend: function () {
            $('.blog-saved').hide();
            $('.blog-saving').show();
        },
        error: function () {
            $('.blog-saving').hide();

        },
        success: function (result) {
            $('.blog-saving').hide();
            $('.blog-saved').show().html('Last autosaved at: ' + new Date().toLocaleString());

            if (!blog_id) {
                console.dir('new blog - setting id to div');
                $('#blog-id').data('id', result.blog_id);

                let url = window.location.href + '/' + result.blog_id;
                // document.title = response.pageTitle;
                window.history.replaceState(null, "", url);
            }
        }
    });
}

require('webpack-jquery-ui/sortable');
require('webpack-jquery-ui/draggable');
require('webpack-jquery-ui/droppable');


$(function () {
    $(".sortable").sortable({
        revert: true,
        // placeholder: "ui-state-highlight",
        handle: '.handle',
        forcePlaceholderSize: true,
        cursor: "move",
        tolerance: 'pointer',

        update: function (event, ui) {
            // var data = $('#blog').serialize();
            var data = $('.sortable').sortable('serialize');

            // POST to server using $.post or $.ajax
            $.ajax({
                data: data,
                type: 'POST',
                url: Routing.generate('blog_update_order'),
                error: function () {
                    console.dir('error')

                },
                success: function (result) {
                    console.dir('saved');

                }
            });
        }
    });
});


$('.btn-blog-delete').on('click', function () {

    let id = $(this).data('id');
    let blog_id = $(this).data('blog-id');
    let $this = $(this);

    console.dir('test');

    $.ajax({
        type: "DELETE",
        url: Routing.generate('blog_delete', {blog_id: blog_id}),

        beforeSend: function () {
        },
        error: function () {
        },
        success: function (result) {
            $this.closest('tr').remove();
        }
    });
});
