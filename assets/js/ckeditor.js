import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

// ClassicEditor.editorConfig = function (config) {
//     config.extraPlugins = 'wordcount,notification';
// };

if (document.getElementsByClassName("editor")) {
    ClassicEditor
        .create(document.querySelector('.editor'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });

}
