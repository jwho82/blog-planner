require('summernote');

$('.editor').summernote({
    popover: {
        image: [],
        link: [],
        air: []
    },
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear', 'color']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['insert', ['link', 'table', 'hr']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['misc', ['undo', 'redo']]
    ],
    placeholder: 'Text Here',
    minHeight: 200,
    maxHeight: 500,
    height: 300
});