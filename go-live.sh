#!/usr/bin/env bash

# sudo /etc/init.d/mysql start
# sudo service apache2 graceful

# ln -s /usr/bin/nodejs /usr/bin/node # creates link

composer install  --optimize-autoloader
yarn install
yarn encore production
#php bin/console cache:warmup
php bin/console doctrine:schema:update --force

php bin/console cache:clear
php bin/console cache:warmup
setfacl -RL -m u:"www-data":rwX -m u:`whoami`:rwX var
