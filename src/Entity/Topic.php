<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="App\Repository\TopicRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Topic
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content = '';


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;


    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $wordCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $position = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime
     */
    private $dateUpdated;

    /**
     * @ORM\ManyToOne(targetEntity="Blog", inversedBy="topics")
     */
    protected $blog;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="topics")
     * @Assert\NotBlank
     */
    protected $user;


    public function __construct()
    {
        $this->dateAdded = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): ? int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Topic
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ? string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Topic
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ? string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Topic
     */
    public function setContent($content)
    {
        $this->content = $content;

        $text = html_entity_decode(strip_tags($content));
        $this->setWordCount(str_word_count($text));
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ? int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Topic
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): ? \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @param \DateTime $dateAdded
     * @return Topic
     */
    public function setDateAdded(\DateTime $dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): ? \DateTime
    {
        return $this->dateUpdated?:$this->getDateAdded();
    }

    /**
     * @param \DateTime $dateUpdated
     * @return Topic
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updateDate()
    {
        $this->dateUpdated = new \DateTime();
        if ($blog = $this->getBlog()) {
            $this->getBlog()->updateDate();
        }
    }


    /**
     * @return Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param mixed $blog
     * @return Topic
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes(): ? string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Topic
     */
    public function setNotes(string $notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): ? int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return Topic
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Topic
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getWordCount(): ? int
    {
        return $this->wordCount;
    }

    /**
     * @param int $wordCount
     * @return Topic
     */
    public function setWordCount(int $wordCount)
    {
        $this->wordCount = $wordCount;
        return $this;
    }

    public function getStatusText()
    {
        switch ($this->getStatus()) {

            case 0: return 'Draft';
            case 1: return 'Pending';
            case 2: return 'In Progress';
            case 3: return 'Needs Review';
            case 4: return 'Ready';
            case 5: return 'Published';
        }
    }



}