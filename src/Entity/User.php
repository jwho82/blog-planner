<?php
// src/AppBundle/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $name;


    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $dateJoined;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $blogs;


    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $topics;

    public function __construct()
    {
        parent::__construct();
        $this->dateJoined = new \DateTime();

    }

    /**
     * @return mixed
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * @param mixed $blogs
     * @return User
     */
    public function setBlogs($blogs)
    {
        $this->blogs = $blogs;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ? string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ? string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->setUsername($email);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param mixed $topics
     * @return User
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateJoined(): ? \DateTime
    {
        return $this->dateJoined;
    }

    /**
     * @param \DateTime $dateJoined
     * @return User
     */
    public function setDateJoined(\DateTime $dateJoined)
    {
        $this->dateJoined = $dateJoined;
        return $this;
    }


}