<?php

namespace App\Entity;

use App\Util\Util;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 * @ORM\Table(name="blog")
 * @ORM\Entity(repositoryClass="App\Repository\BlogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Blog
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $keyword;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $dateAdded;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime
     */
    private $dateUpdated;

    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="blog", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $topics;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="blogs")
     * @Assert\NotBlank
     */
    protected $user;

//    /**
//     * @ORM\ManyToOne(targetEntity="User", mappedBy="blogs")
//     */
//    protected $user;

    public function __construct()
    {
        $this->dateAdded = new \DateTime();
        $this->topics = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Blog
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Blog
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Blog
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Blog
     */
    public function setNotes(string $notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): ?\DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @param \DateTime $dateAdded
     * @return Blog
     */
    public function setDateAdded(\DateTime $dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Blog
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): ?\DateTime
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     * @return Blog
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    /**
     */
    public function updateDate()
    {
        $this->dateUpdated = new \DateTime();
    }

    /**
     * @return ArrayCollection[]|Topic[]
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param mixed $topics
     * @return Blog
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Blog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     * @return Blog
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     * @return Blog
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    public function getStatusText()
    {
        switch ($this->getStatus()) {

            case 6:
                return 'Archived';
            case 0:
                return 'Draft';
            case 1:
                return 'Pending';
            case 2:
                return 'In Progress';
            case 3:
                return 'Needs Review';
            case 4:
                return 'Ready';
            case 5:
                return 'Published';
        }
    }

    /**
     * Get the word count just for the summary
     *
     * @return mixed
     */
    public function getBlogWordCount()
    {
        $text = html_entity_decode(strip_tags($this->getSummary()));

        return str_word_count($text);
    }

    /**
     * Get the word count for the summary and all the topics combined
     *
     * @return int
     */
    public function getTotalWordCount()
    {
        $text = html_entity_decode(strip_tags($this->getSummary()));

        $total = str_word_count($text);

        foreach ($this->getTopics() as $topic) {
            $total += $topic->getWordCount();
        }

        return $total;
    }

    public function getReadingTime()
    {
        $text = $this->getTotalWordCount();

        return Util::readingTime($text);
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return Blog
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
        return $this;
    }

    public function getPreview()
    {

        $text = $this->getTitle();

        if ($summary = $this->getSummary()) {
            $text .= "<br>$summary";
        }

        foreach ($this->getTopics() as $topic) {
            $text .="<br>".$topic->getTitle();
            $text .="<br>".$topic->getContent();
        }

        return $text;

    }


}
