<?php

namespace App\Repository;

use App\Entity\Blog;

class TopicRepository extends \Doctrine\ORM\EntityRepository
{
    public function getTopics(Blog $blog)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.blog', 'b')
            ->where('b = :blog')
            ->setParameter('blog', $blog)

            ->orderBy('t.position')
        ;

        return $qb->getQuery()->getResult();

    }

}