<?php
namespace App\Repository;

use App\Entity\Blog;
use App\Entity\User;

class BlogRepository extends \Doctrine\ORM\EntityRepository
{
    // 0 all, 1 published, 2 archived
    public function getBlogs(User $user, $type = 0)
    {
        $qb = $this->createQueryBuilder('b')
            ->join('b.user', 'u')
            ->where('u = :user')
            ->setParameter('user', $user)

            ->orderBy('b.position')
        ;
        if ($type == 1) {
            $qb = $qb->andWhere('b.status = 5');
        } elseif ($type == 0) {
            $qb = $qb->andWhere('b.status < 5');
        } else {
            $qb = $qb->andWhere('b.status = 6');
        }

        return $qb->getQuery()->getResult();

    }

}
