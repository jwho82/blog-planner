<?php

namespace App\Util;


class Util
{

    static function readingTime($txt)
    {
        $m = floor($txt / 200);
        $s = floor($txt % 200 / (200 / 60));
        $est = $m . ' minute' . ($m == 1 ? '' : 's') . ', ' . $s . ' second' . ($s == 1 ? '' : 's');

        return $est;
    }
}