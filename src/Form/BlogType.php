<?php

namespace App\Form;

use App\Entity\Blog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'attr' => ['class' => 'wordcount']
            ])
            ->add('keyword')
            ->add('meta_description', TextareaType::class, [
                'attr' => ['rows' => 3, 'class' => 'wordcount']
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Archived' => 6,
                    'Draft' => 0,
                    'Pending' => 1,
                    'In Progress' => 2,
                    'Needs Review' => 3,
                    'Ready' => 4,
                    'Published' => 5,
                ]
            ])
            ->add('notes', null, [
                'attr' => ['rows' => 6]
            ])
            ->add('summary', TextareaType::class, [
                'label' => 'Intro Paragraph',
                'attr' => ['rows' => 10, 'class' => 'editor']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }

}
