<?php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends BaseType
{
    private $class;

    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, array(
                    'error_bubbling' => true,
                    'attr' => array('placeholder' => 'Full Name')
                )
            )

            ->add('email', EmailType::class, array(
                    'error_bubbling' => true,
                    'attr' => array('placeholder' => 'E-mail Address')
                )
            )

            ->add('plainPassword', PasswordType::class, array(
                    'error_bubbling' => true,
//                    'type' => 'password',
//                    'options' => array('translation_domain' => 'FOSUserBundle'),
//                    'first_options' => array('attr' => array('placeholder' => 'Password')),
//                    'second_options' => array('attr' => array('placeholder' => 'Verify Password')),
                    'invalid_message' => 'fos_user.password.mismatch',
                )
            );


        //parent::buildForm($builder, $options); // imports FOS crap we dont need

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_token_id' => 'registration',
        ));


//        $resolver->setDefaults(array(
//            'data_class' => User::class,
//        ));
    }

}