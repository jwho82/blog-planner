<?php
// Pre-deployment tasks
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


class TestCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('deploy:test')
            ->setDescription('Update source, database, entities, etc.');
    }

    // https://github.com/symfony/symfony/pull/25117
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $this->getContainer()->get('kernel')->getRootDir() . "/../";
        try {
//            exec("php bin/console cache:clear");
//            exec("rm -rf var/cache");

//            $this->getApplication()->setDispatcher(new EventDispatcher());

            $process = new Process("cd $path && ./go-live.sh");
            $process->run();
            $results = $process->getOutput();


            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

        } catch (\Exception $e) {

            return "error";
        }

        return $results;

    }
}
