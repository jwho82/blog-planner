<?php

namespace App\User\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewController extends Controller
{
    /**
     * New Registrations land here
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function emailConformationAction(Request $request)
    {

        $user = $this->getUser();
        // Ignore admins

        $userManager = $this->get('fos_user.user_manager');

        if ($user->getConfirmationToken() === NULL) {
            $user->getEmailConfirmed(true);
            $userManager->updateUser($user);

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Your e-mail has been confirmed'
            );
        }

        if ($user->isEmailConfirmed()) {
            return $this->redirect($this->generateUrl('profile_edit'));
        }

        $mailer = $this->container->get('fos_user.mailer');
        $token = sha1(uniqid(mt_rand(), true)); // Or whatever you prefer to generate a token

        $user->setConfirmationToken($token);

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $mailer->sendConfirmationEmailMessage($user);

        return $this->redirect($this->generateUrl('profile_edit', array('new' => true)));


    }

}