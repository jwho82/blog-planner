<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/panel/admin", name="admin_")
 */
class AdminController extends Controller
{

    /**
     * @param Request $request
     * @Route("/users", name="users")
     */
    public function userAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App:User')->findAll();

        return $this->render('admin/users.html.twig', [
            'users' => $users
        ]);

    }

    /**
     * @param Request $request
     * @Route("/users/view/{user_id}", name="view_user")
     */
    public function viewAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find(
            $user_id
        );


        return $this->render('admin/view_user.html.twig', [
            'user' => $user
        ]);

    }


    /**
     * @param Request $request
     * @Route("/users/delete/{user_id}", name="delete_user", options={"expose": true})
     */
    public function deleteAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find(
            $user_id
        );

        if ($user === $this->getUser())
            return new JsonResponse(['status' => 'false']);


        $em->remove($user);
        $em->flush();

        return new JsonResponse(['status' => 'success']);

    }



    /**
     * @param Request $request
     * @Route("/deploy", name="deploy", options={"expose": true})
     */
    public function deploy(Request $request)
    {
        $output = '';
        $output2 = '';

        $process = new Process('cd .. && /usr/bin/git fetch --all');
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = trim($process->getOutput()."\n\n ||");



        $process = new Process('cd .. && /usr/bin/git status');
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output .= trim($process->getOutput()."\n\n");

        $process = new Process('cd .. && /usr/bin/git rev-parse --abbrev-ref HEAD');
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $branch = trim($process->getOutput());

        // todo check for changes. If none, do not run
        if ($request->isMethod('post')) {


//            $process = new Process('cd .. &&  ./vendor/bin/dep deploy localhost -v');
//
//            ob_start();
//            echo "<pre>";
//
//            $process->run(function ($type, $buffer) {
//                ob_flush();
//                flush();
//
//                if (Process::ERR === $type) {
//                    echo 'ERR > '.$buffer;
//                } else {
//                    echo "<>".$buffer;
//                }
//            });
//
//            echo "</pre>";
//
//            exit;


            if ($this->get('kernel')->getEnvironment() != 'prod') {

                $process = new Process('cd .. && php bin/console deploy:test');
                $process->run();
                $output2 = $process->getOutput();

                return $this->render('admin/_deploy_status.html.twig', [
                    'branch' => 'Only will run on Prod',
                    'output' => $output2,
                    'output2' => $output2
                ]);
            }

            set_time_limit(0);
            $git_bin_path = '/usr/bin/git';

            $process = new Process('cd .. && /usr/bin/git reset --hard origin/master');
            $process->run();
//
            while ($process->isRunning()) {
                // waiting for process to finish
            }
//
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
//
            $output = $process->getOutput();

            $process2 = new Process('cd .. && ./go-live.sh');
            $process2->run();

            while ($process2->isRunning()) {
                // waiting for process to finish
            }
//
            if (!$process2->isSuccessful()) {
                throw new ProcessFailedException($process2);
            }

            $output2 = $process2->getOutput();

            return $this->render('admin/_deploy_status.html.twig', [
                'branch' => $branch,
                'output' => $output,
                'output2' => $output2
            ]);

        }


        return $this->render('admin/deploy.html.twig', [
            'branch' => $branch,
            'output' => $output,
            'output2' => $output2
        ]);
    }

}
