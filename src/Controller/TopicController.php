<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Entity\Topic;
use App\Form\BlogType;
use App\Form\TopicType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/panel/blog/{blog_id}", name="topic_")
 */
class TopicController extends Controller
{

    /**
     * @param Request $request
     * @Route("/topic", name="home")
     */
    public function indexAction($blog_id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('App:Blog')->find($blog_id);
        $topics = $em->getRepository('App:Topic')->getTopics($blog);

        return $this->render('topic/index.html.twig', [
            'blog' => $blog,
            'topics' => $topics,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/topic/edit/{topic_id}", name="edit", options={"expose": true})
     */
    public function editAction($blog_id, $topic_id = null, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('App:Blog')->find($blog_id);

        $topic = new Topic();
        $topic->setUser($this->getUser());
        $topic->setBlog($blog);

        if ($topic_id) {
            $topic = $em->getRepository('App:Topic')->findOneBy(['blog' => $blog, 'id' => $topic_id]);
            $topic->setUser($this->getUser());
        }

        if (!$topic->getId() && $lastTopic = $blog->getTopics()->last()) {
            $position = $lastTopic->getPosition() + 1;
            $topic->setPosition($position);
        }

        $form = $this->createForm(TopicType::class, $topic);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($topic);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'topic_id' => $topic->getId()]);
            }

            return $this->redirectToRoute('topic_home', ['blog_id' => $blog_id]);

        }


        $topics = $em->getRepository('App:topic')->findAll();

        return $this->render('topic/edit.html.twig', [
            'form' => $form->createView(),
            'topics' => $topics
        ]);

    }

    /**
     * @Route("/topic/order", name="update_order", options={"expose": true}, methods={"POST"})
     */
    public function updateOrderAction($blog_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('App:Blog')->find($blog_id);

        if (!$blog)
            throw new BadRequestHttpException('Cannot find that blog');


        $topics = $request->request->get('topic');

        foreach ($blog->getTopics() as $topic) {

            $position = array_search($topic->getId(), $topics);
            $topic->setPosition($position);
            $em->persist($topic);
        }

        $em->flush();
        return new JsonResponse([$request->request->all()]);


    }

    /**
     * @Route("/topic/delete/{topic_id}", name="delete", options={"expose": true}, methods={"DELETE"})
     */
    public function deleteAction($topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $topic = $em->getRepository('App:Topic')->findOneBy([
            'user' => $this->getUser(),
            'id' => $topic_id
        ]);

        if (!$topic)
            throw new NotFoundHttpException();

        $em->remove($topic);
        $em->flush();

        return new JsonResponse(['status' => 'success']);

    }

}