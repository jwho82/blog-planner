<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\AccountType;
use App\Form\BlogType;
use App\Form\ContactType;
use App\Service\EmailService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/panel/account", name="account_")
 */
class AccountController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createForm(AccountType::class, $this->getUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Update Password
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($this->getUser());

            $em->persist($this->getUser());
            $em->flush();

            return $this->redirectToRoute('blog_home');

        }



        return $this->render('account/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request, EmailService $emailService)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //todo send email

            $emailService->contact();

            return $this->redirectToRoute('blog_home');

        }


        return $this->render('home.html.twig', [
            'form' => $form->createView()
        ]);
    }
}