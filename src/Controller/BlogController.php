<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panel/blog", name="blog_")
 */
class BlogController extends Controller
{

    /**
     * @param Request $request
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $draftBlogs = $em->getRepository('App:Blog')->getBlogs($this->getUser());
        $publishedBlogs = $em->getRepository('App:Blog')->getBlogs($this->getUser(), 1);
        $archivedBlogs = $em->getRepository(Blog::class)->getBlogs($this->getUser(), 2);

        return $this->render('blog/index.html.twig', [
            'draftBlogs' => $draftBlogs,
            'publishedBlogs' => $publishedBlogs,
            'archivedBlogs' => $archivedBlogs
        ]);
    }

    /**
     * @param Request $request
     * @Route("/edit/{blog_id}", name="edit", options={"expose": true})
     */
    public function editAction($blog_id = null, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = new Blog();
        $blog->setUser($this->getUser());
        $blog->setUser($this->getUser());
        if ($blog_id) {
            $blog = $em->getRepository('App:Blog')->findOneBy([
                'id' => $blog_id,
                'user' => $this->getUser()
            ]);
        }

        $form = $this->createForm(BlogType::class, $blog);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $blog->updateDate();
            $em->persist($blog);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'blog_id' => $blog->getId()]);
            }

            return $this->redirectToRoute('topic_home', ['blog_id' => $blog->getId()]);

        }

        return $this->render('blog/edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/preview/{blog_id}", name="preview")
     */
    public function previewAction($blog_id)
    {

        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository(Blog::class)->find($blog_id);
        $text = $blog->getPreview();

        $key = "LGUKO3Z56MCZ2WEY8J78AIP23116NYPU";

        $url = 'https://api.readable.com/api/text/'; // The API endpoint you want to interact with
        $request_time = time(); // Time of request
        $api_signature = md5($key . $request_time);

        $postItems = ['text' => $text];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);


        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postItems));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('API_SIGNATURE: ' . $api_signature, 'API_REQUEST_TIME: ' . $request_time));
        $file = curl_exec($ch);
        curl_close($ch);
//
        $results = json_decode($file, true);

        return $this->render('blog/preview.html.twig', [
            'blog' => $blog,
            'results' => $results
        ]);

    }

    /**
     * @Route("/order", name="update_order", options={"expose": true}, methods={"POST"})
     */
    public function updateOrderAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository(Blog::class)->getBlogs($this->getUser());
        $orderedBlogs = $request->request->get('blog');

        /** @var Blog $blog */
        foreach ($blogs as $blog) {

            $position = array_search($blog->getId(), $orderedBlogs);
            $blog->setPosition($position);
            $em->persist($blog);

        }

        $em->flush();
        return new JsonResponse([$request->request->all()]);


    }

    /**
     * @Route("/delete/{blog_id}", name="delete", options={"expose": true}, methods={"DELETE"})
     */
    public function deleteAction($blog_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository(Blog::class)->findOneBy([
            'user' => $this->getUser(),
            'id' => $blog_id
        ]);

        if (!$blog)
            throw new NotFoundHttpException();

        $em->remove($blog);
        $em->flush();

        return new JsonResponse(['status' => 'success']);

    }



}
