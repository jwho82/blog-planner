<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends Controller
{

    /**
     * @param Request $request
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $response =  $this->render('home.html.twig', []);
        $response->setSharedMaxAge(3600);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        return $response;
    }

    /**
     * @param Request $request
     * @Route("/features", name="features")
     */
    public function featuresAction(Request $request)
    {
        return $this->render('home.html.twig', [
        ]);
    }

    /**
     * @Route("/sitemap.{_format}", name="sitemap")
     */
    public function sitemapAction(Request $request)
    {

        $urls = [];
        $hostname = 'https://'.$request->getHost();

        $urls[] = array('loc' => $this->get('router')->generate('home'), 'changefreq' => 'daily', 'priority' => '1.0');


        return $this->render('sitemap.xml.twig', array(
            'urls' => $urls,
            'hostname' => $hostname
        ));
    }

}
