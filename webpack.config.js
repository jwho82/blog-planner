var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .autoProvideVariables({
        // "window.Bloodhound": require.resolve('bloodhound-js'),
        Popper: ['popper.js', 'default'],
        'window.jQuery': 'jquery',
        $: 'jquery',
        jQuery: 'jquery',
    })


    // .enableVueLoader(function(options) {
    //     // https://vue-loader.vuejs.org/en/configurations/advanced.html
    //
    //     // options.preLoaders = {
    //     //     js: 'assets/vue/test.vue'
    //     // };
    // })

    .enableSassLoader()
    .enableVersioning(Encore.isProduction())
    .createSharedEntry('js/common', ['jquery'])
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/routing', './assets/js/routing.js')
    // .addEntry('js/login', './assets/js/login.js')
    .addEntry('js/topic', './assets/js/topic.js')
    .addEntry('js/blog', './assets/js/blog.js')
    .addEntry('js/admin', './assets/js/admin.js')
    // .addEntry('js/component.js', './assets/js/theme/Component.js')
    // .addEntry('js/config.js', './assets/js/theme/Config.js')
    // .addEntry('js/plugin.js', './assets/js/theme/Plugin.js')
    // .addEntry('js/base.js', './assets/js/theme/Base.js')


    .addStyleEntry('css/app', ['./assets/scss/app.scss'])
    // .addStyleEntry('css/admin', ['./assets/scss/admin.scss'])


;

Encore.configureExtractTextPlugin((options) => {
    options.ignoreOrder = true;
});

// delete old files when done instead of before
Encore.cleanupOutputBeforeBuild(['**/*.js', '**/*.css'], (options) => {
    options.beforeEmit = Encore.isProduction()
});

module.exports = Encore.getWebpackConfig();
